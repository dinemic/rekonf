#ifndef REKONFAPP_H
#define REKONFAPP_H

#include <libdinemic/dapp.h>

#include <iostream>

#include <models/configadmin.h>
#include <models/configfile.h>
#include <models/confignode.h>

#include <listeners/monitorfilechanges.h>

class RekonfApp : public Dinemic::DApp
{
    template <class Model>
    void list_objects(std::string model_name);

    void node_create();
    void admin_create();
    void file_create();
    void list_owned_files();

    MonitorFileChanges monitor_file_changes;

public:
    RekonfApp(int argc, char **argv);

    /// This mode is used to create node, admin or file objects in cluster
    void create();

    /// Launch Konfigurator application to listen for changes in cluster. Should
    /// be launched on each node
    void launch();

    /// Edit file contents, assign admins to files and files to nodes
    void oneshot();
};

#endif // REKONFAPP_H
