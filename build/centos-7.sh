yum install -y \
        git \
	wget \
	rsync \
        python \
        tree \
	rpm \
	rpm-build \
	uuid-runtime \
	ruby-devel \
	rubygems \
	gcc \
	gcc-c++ \
	pkg-config \
	redhat-lsb-core \
	curl \
	wget
	
libdinemic_dir=`pwd`
cd /tmp

wget https://cmake.org/files/v3.10/cmake-3.10.1.tar.gz
tar xf cmake-3.10.1.tar.gz

cd cmake-3.10.1

./configure
make -j 4
make install

cd ..

gem install fpm

wget -O libdinemic.rpm https://packages.dinemic.io/nightly/centos-7/current/`curl https://packages.dinemic.io/nightly/centos-7/current/ 2>/dev/null | grep "\.rpm" | cut -d '"' -f 2`
rpm -ivh libdinemic.rpm
