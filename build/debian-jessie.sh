#echo "deb http://ftp.debian.org/debian jessie-backports main" >> /etc/apt/sources.list
apt-get update
apt-get install --yes \
        git \
	rsync \
        net-tools \
        dpkg-dev \
        lintian \
        python \
        debhelper \
        dh-systemd \
        tree \
        aptly \
	uuid-runtime \
	ruby-dev \
	rubygems \
	gcc-4.8 \
	g++-4.8 \
	pkg-config \
	lsb-release \
	cmake \
	curl \
	wget
#apt-get install -t jessie-backports --yes cmake

libdinemic_dir=`pwd`
cd /tmp

wget https://cmake.org/files/v3.10/cmake-3.10.1.tar.gz
tar xf cmake-3.10.1.tar.gz

cd cmake-3.10.1

./configure
make -j 4
make install

cd $libdinemic_dir

gem install fpm
wget -O libdinemic.deb https://packages.dinemic.io/nightly/debian-jessie/current/`curl https://packages.dinemic.io/nightly/debian-jessie/current/ 2>/dev/null | grep "\.deb" | cut -d '"' -f 2`
dpkg -i libdinemic.deb
