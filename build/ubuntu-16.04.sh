apt-get update
apt-get install --yes \
        git \
	rsync \
        net-tools \
        supervisor \
        dpkg-dev \
        lintian \
        python \
        debhelper \
        dh-systemd \
        tree \
        aptly \
	rpm \
	uuid-runtime \
	ruby-dev \
	gcc \
	g++ \
	pkg-config \
	lsb-release \
	cmake \
	curl \
	wget
gem install fpm

wget -O libdinemic.deb https://packages.dinemic.io/nightly/ubuntu-16.04/current//`curl https://packages.dinemic.io/nightly/ubuntu-16.04/current/ 2>/dev/null | grep "\.deb" | cut -d '"' -f 2`
dpkg -i libdinemic.deb
