yum update -y
yum install -y \
        git \
	wget \
	rsync \
        python36 \
        python27 \
        tree \
	rpm \
	rpm-build \
	ruby-devel \
	rubygems \
	gcc \
	gcc-c++ \
	pkg-config \
	redhat-lsb-core \
	git \
	cmake

ln -s /usr/bin/python3 /usr/bin/python
gem install fpm

wget -O libdinemic.rpm https://packages.dinemic.io/nightly/centos-8/current/`curl https://packages.dinemic.io/nightly/centos-8/current/ 2>/dev/null | grep "\.rpm" | cut -d '"' -f 2`
rpm -ivh libdinemic.rpm
