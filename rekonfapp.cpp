#include "rekonfapp.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

RekonfApp::RekonfApp(int argc, char **argv)
    : DApp(argc, argv, "Rekonf", "Dinemic-based application for configuration sync")
{
    // General
    add_option("A", "admin-id", "Admin ID", false, DAppOptionMode_any);
    add_option("F", "file-id", "File ID", false, DAppOptionMode_any);
    add_option("N", "node-id", "Node ID", false, DAppOptionMode_any);

    // Create admin
    add_option("a", "admin-create", "Create new admin", true, DAppOptionMode_create);
    add_option("", "admin-name", "Admin name", false, DAppOptionMode_create);
    add_option("", "admin-email", "Admin email", false, DAppOptionMode_create);
    add_option("", "admin-list", "List all known admins", true, DAppOptionMode_oneshot);

    // Manage files
    add_option("c", "file-create", "Define new file with given path", false, DAppOptionMode_create);
    add_option("e", "file-edit", "Edit file contents. Data will be read from stdin. Requires --file-id option", false, DAppOptionMode_oneshot);
    add_option("s", "file-show", "Show file contents stored in database, on this node. Requires --file-id option", true, DAppOptionMode_oneshot);
    add_option("x", "file-pre-update", "Set command to be executed before file contents are changed", false, DAppOptionMode_oneshot);
    add_option("X", "file-post-update", "Set command to be executed after file contents has been changed", false, DAppOptionMode_oneshot);
    add_option("l", "file-list", "List all known files", true, DAppOptionMode_oneshot);
    add_option("d", "file-delete", "Remove file", false, DAppOptionMode_oneshot);
    add_option("", "file-list-owned", "Show owned files on this node", true, DAppOptionMode_oneshot);

    // Manage nodes
    add_option("n", "node-create", "Create new node on this machine, if doesn't exist", true, DAppOptionMode_create);
    add_option("", "node-add-file", "Assign file to node. Select node by multiple options --node-id and file by --file-id", true, DAppOptionMode_oneshot);
    add_option("", "node-detach-file", "Stop synchronizing file on node. Use --node-id to select node and --file-id to select file", true, DAppOptionMode_oneshot);
    add_option("", "node-list", "List all known nodes", true, DAppOptionMode_oneshot);

    // Add admin to file
    add_option("", "file-add-admin", "Assign admin to file. Pass admin id by this option's parameter and file ids by multiple options --file-id. Use --admin-id option to make this edit as your acount", false, DAppOptionMode_oneshot);
    add_option("", "file-remove-admin", "Revoke admin from file. Select admin by --admin-id and file by multiple options --file-id", true, DAppOptionMode_oneshot);


    char *local_config = getenv("DINEMIC_CONFIG");
    if (local_config == NULL) {
        config.open("/etc/rekonf/config.dinemic");
    }

    monitor_file_changes.apply(sync);
}

template <class Model>
void RekonfApp::list_objects(string model_name) {
    vector<string> objects = Model::object_list(model_name, store);

    DModel *caller = NULL;
    if (has_option("admin-id")) {
        caller = new ConfigAdmin(get_option("admin-id").values[0], store, sync);
    } else if (has_option("node-id")) {
        caller = new ConfigNode(get_option("node-id").values[0], store, sync);
    } else if (has_option("file-id")) {
        caller = new ConfigFile(get_option("file-id").values[0], store, sync);
    }

    cout << "[" << endl;
    for (int i = 0; i < objects.size(); i++) {
        Model object(objects[i], store, sync, caller);
        cout << object.serialize();
        if (i < objects.size()-1) {
            cout << ",";
        }
        cout << endl;
    }
    cout << "]" << endl;

    if (caller != NULL) {
        delete caller;
    }
}

void RekonfApp::node_create() {
    vector<string> owned_nodes = ConfigNode::object_list_owned("ConfigNode:[id]");
    if (owned_nodes.size() > 0) {
        cerr << "An instance of ConfigNode exists on this machine" << endl;
        cout << owned_nodes[0] << endl;
        return;
    }

    vector<string> authorized_objects;
    if (has_option("admin-id")) {
        for (auto id : get_option("admin-id").values) {
            authorized_objects.push_back(id);
        }
    }

    ConfigNode node(store, sync, authorized_objects);
    cout << node.get_db_id() << endl;
}

void RekonfApp::admin_create() {
    vector<string> owned_admins = ConfigAdmin::object_list_owned("ConfigAdmin:[id]");
    if (owned_admins.size() > 0) {
        cerr << "An instance of ConfigAdmin exists on this machine" << endl;
        cout << owned_admins[0] << endl;
        return;
    }

    if (!has_option("admin-name")) {
        cerr << "Missing option --admin-name" << endl;
        return;
    }

    if (!has_option("admin-email")) {
        cerr << "Missing option --admin-email" << endl;
        return;
    }
    ConfigAdmin admin(store, sync, vector<string>());

    cout << admin.get_db_id() << endl;
}

void RekonfApp::file_create() {
    vector<string> authorized_objects;
    if (has_option("admin-id")) {
        for (auto id : get_option("admin-id").values) {
            authorized_objects.push_back(id);
        }
    }
    ConfigFile file(store, sync, authorized_objects);

    if (has_option("file-pre-update")) {
        file.pre_update_command = get_option("file-pre-update").values[0];
    }
    if (has_option("file-post-update")) {
        file.post_update_command = get_option("file-post-update").values[0];
    }

    file.path = get_option("file-create").values[0];

    cout << file.get_db_id() << endl;
}

void RekonfApp::list_owned_files() {
    vector<string> files = DModel::object_list_owned("ConfigFile:*");
    cout << "[" << endl;
    for (int i = 0; i < files.size(); i++) {
        ConfigFile file_object(files[i], get_store_interface(), get_sync_interface());
        cout << "    \"" << file_object.path.to_string() << "\"";
        if (i == files.size() - 1) {
            cout << endl;
        } else {
            cout << "," << endl;
        }
    }
    cout << "]";
}

void RekonfApp::create() {
    if (has_option("admin-create")) {
        admin_create();
        return;
    }

    if (has_option("node-create")) {
        node_create();
        return;
    }

    if (has_option("file-create")) {
        file_create();
        return;
    }

    cerr << "Missing option for create. See --help" << endl;
}

void RekonfApp::oneshot() {
    if (has_option("file-list")) {
        list_objects<ConfigFile>("ConfigFile:[id]");
        return;
    }
    if (has_option("admin-list")) {
        list_objects<ConfigAdmin>("ConfigAdmin:[id]");
        return;
    }
    if (has_option("node-list")) {
        list_objects<ConfigNode>("ConfigNode:[id]");
        return;
    }

    if (has_option("file-list-owned")) {
        list_owned_files();
        return;
    }

    if (!has_option("admin-id")) {
        cerr << "Missing option --admin-id" << endl;
        return;
    }

    ConfigAdmin admin(get_option("admin-id").values[0], store, sync);
    if (!admin.is_owned()) {
        cerr << "You are not this admin!" << endl;
        return;
    }

    // Get file object
    if (!has_option("file-id")) {
        cerr << "Missing option --file-id" << endl;
        return;
    }
    ConfigFile file(get_option("file-id").values[0], store, sync, &admin);

    // Handle edits
    if (has_option("file-edit")) {
        file.contents = get_option("file-edit").values[0];
        cout << "File contents edited. Path: " << file.path.to_string() << endl;
        return;
    }

    // Handle show
    if (has_option("file-show")) {
        if (!file.is_read_authorized(admin.get_db_id())) {
            cerr << "You are not authorized to read this file (not authorized admin or missing encryption keys)" << endl;
            return;
        }
        cout << file.contents.to_string() << endl;
    }

    // Set pre-edit command for file
    if (has_option("file-pre-update")) {
        file.pre_update_command = get_option("file-pre-update").values[0];
        cout << "Pre update command changed" << endl;
    }
    // Set post-edit command for file
    if (has_option("file-post-update")) {
        file.post_update_command = get_option("file-post-update").values[0];
        cout << "Post update command changed" << endl;
    }

    // Add file to node(s)
    if (has_option("node-add-file")) {
        if (!has_option("node-id")) {
            cerr << "Missing option --node-id" << endl;
        }

        for (string node : get_option("node-id").values) {
            ConfigNode config_node(node, store, sync, &admin);
            config_node.files.append(file.get_db_id());

            // Node should get only access to reading file contents. No updates
            // Add node's id to read authorized objects list and reencrypt all
            // encrypted data again, to make it readable by new node too.
            file.append_read_authorized(config_node.get_db_id());

            cout << "Added file " << file.get_db_id() << " to node " << config_node.get_db_id() << endl;
        }
        return;
    }

    /// Detach file from node
    /// // TODO

    // Add admin to file(s)
    if (has_option("file-add-admin")) {
        for (string f : get_option("file-id").values) {
            ConfigFile config_file(f, store, sync, &admin);
            // Add object id to read and update authorized objects lists
            // and reencrypt all encrypted data again, to make it available to
            // new admin too.
            config_file.append_read_authorized(get_option("file-add-admin").values[0]);
            config_file.append_update_authorized(get_option("file-add-admin").values[0]);

            cout << "Added admin " << get_option("file-add-admin").values[0] << " to file " << file.get_db_id() << endl;
        }
    }
}

void RekonfApp::launch() {
}
