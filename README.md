Rekonf
======
Warning: this project is still under development.

This is simple application for synchronizing config files across nodes, in
decentralized way, based on dinemic framework.

Compilation
-----------
Get latest version of dinemic library for your favorite distribution:

```https://packages.dinemic.io/```

and install proper package. Then compile konfigurator from sources:
```
cmake .
make
sudo cp rekonf /usr/bin/
```

Usage
=====
Preparation
-----------
On each node and your local machine install redis-server and copy config.dinemic
to directory, where application will be launched. Then launch konfigurator as
background process at each node and your machine with --launch option:

```./rekonf --launch &```

Creating administrator
----------------------
Next step is to create admin, which will be authorized to perform actions on
each node (we still have --launch mode on all machines in cluster!). Launch only
at your machine:

```./rekonf --create --admin-create --admin-name Me --admin-email me@me.com```

This will create key pair on machine where it was launched. Store somewhere
string with admin id (ConfigAdmin:349uorqi3rjo...). We will need it later.

Creating nodes in database
--------------------------
Then, create new  keypair on each node:

```./rekonf --create --node-create --admin-id ConfigAdmin:349uorqi3rjo...```

This command should print ID of each created node, like ConfigNode:293r8... .
Store this IDs too. Adding --admin-id option will authorize any modifications of
this node signed by cryptogarphic key related to this ID. Also this person will
be able too encrypt private data for this node.

Creating files
--------------

Let's go back to your machine. Create new file entry in database. Here you have
admin certificates, so we also can add --admin-id option here to authorize any
administrators to modify this file:

```./rekonf --create --file-create /tmp/test_file --admin-id ConfigAdmin:349uorqi3rjo...```

Again, store ID of new file. Now let's add this file to one of our nodes. Mind
the --oneshot mode this time:

```
./rekonf --oneshot \
         --admin-id ConfigAdmin:349uorqi3rjo... \
         --node-id ConfigNode:293r8... \
         --file-id ConfigFile:aslesl8r... \
         --node-add-file
```

By setting --admin-id option we define who will sign this update of node. By
option --node-id we will set node, which should monitor changes of file. Finally
by option --file-id we can select which file should be stored on node.

From now each edit of file in database will cause that its contents will be
updated. Using dinemic framework causes, that even this modification is done
when node is offline, after reconnecting it to cluster it will get all
modifications instantly, even without presence of your machine. All other nodes
will store all digitally signed updates of database and will share it after
rejoin.

Edit file
---------
To edit contents of file execute:
```
./rekonf --oneshot \
         --admin-id ConfigAdmin:349uorqi3rjo... \
         --file-id ConfigFile:aslesl8r... \
         --file-edit "New contents of file"
```
All nodes with file associated should get this change in couple of seconds.
