#ifndef CONFIGADMIN_H
#define CONFIGADMIN_H

#include <libdinemic/dmodel.h>
#include <libdinemic/dfield.h>

class ConfigAdmin : public Dinemic::DModel
{
public:
    ConfigAdmin(Dinemic::Store::StoreInterface *store,
                Dinemic::Sync::SyncInterface *sync,
                const std::vector<std::string> &authorized_object_ids);
    ConfigAdmin(const std::string &db_id, Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync, Dinemic::DModel *caller=NULL);

    Dinemic::DField name;
    Dinemic::DField email;
};

#endif // CONFIGADMIN_H
