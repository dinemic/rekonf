#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#include <libdinemic/dmodel.h>
#include <libdinemic/dfield.h>

class ConfigFile : public Dinemic::DModel
{
public:
    ConfigFile(Dinemic::Store::StoreInterface *store,
               Dinemic::Sync::SyncInterface *sync,
               const std::vector<std::string> &authorized_object_ids);
    ConfigFile(const std::string &db_id, Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync, DModel *caller=NULL);

    Dinemic::DField pre_update_command;
    Dinemic::DField post_update_command;
    Dinemic::DField contents;
    Dinemic::DField path;
};

#endif // CONFIGFILE_H
