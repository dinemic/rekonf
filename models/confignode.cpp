#include "confignode.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

ConfigNode::ConfigNode(const string &db_id, StoreInterface *store, SyncInterface *sync, DModel *caller)
    : DModel(db_id, store, sync, NULL, caller),
      files(this, "files", true),
      file_status(this, "file_status", true),
      file_pre_actions(this, "file_pre_actions", true),
      file_post_actions(this, "file_post_actions", true)
{

}

ConfigNode::ConfigNode(StoreInterface *store, SyncInterface *sync, const std::vector<string> &authorized_object_ids)
    : DModel("ConfigNode", store, sync, authorized_object_ids),
      files(this, "files", true),
      file_status(this, "file_status", true),
      file_pre_actions(this, "file_pre_actions", true),
      file_post_actions(this, "file_post_actions", true)
{

}
