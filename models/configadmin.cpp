#include "configadmin.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

ConfigAdmin::ConfigAdmin(const string &db_id, StoreInterface *store, SyncInterface *sync, DModel *caller)
    : DModel(db_id, store, sync, NULL, caller),
      name(this, "name", false),
      email(this, "email", false)
{

}

ConfigAdmin::ConfigAdmin(StoreInterface *store, SyncInterface *sync, const std::vector<string> &authorized_object_ids)
    : DModel("ConfigAdmin", store, sync, authorized_object_ids),
      name(this, "name", false),
      email(this, "email", false)
{

}
