#ifndef CONFIGNODE_H
#define CONFIGNODE_H

#include <libdinemic/dmodel.h>
#include <libdinemic/dlist.h>
#include <libdinemic/ddict.h>

class ConfigNode : public Dinemic::DModel
{
public:
    ConfigNode(Dinemic::Store::StoreInterface *store,
               Dinemic::Sync::SyncInterface *sync,
               const std::vector<std::string> &authorized_object_ids);
    ConfigNode(const std::string &db_id, Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync, DModel *caller=NULL);

    /// List of ConfigFiles
    Dinemic::DList files;

    /// Last update ID that updated each file
    Dinemic::DDict file_status;

    /// Outputs of pre update scripts for each file
    Dinemic::DDict file_pre_actions;

    /// Outputs of post update scripts for each file
    Dinemic::DDict file_post_actions;
};

#endif // CONFIGNODE_H
