#include "configfile.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

ConfigFile::ConfigFile(const string &db_id, StoreInterface *store, SyncInterface *sync, DModel *caller)
    : DModel(db_id, store, sync, NULL, caller),
      contents(this, "contents", true),
      path(this, "path", true),
      pre_update_command(this, "pre_update_command", true),
      post_update_command(this, "post_update_command", true)
{

}

ConfigFile::ConfigFile(StoreInterface *store, SyncInterface *sync, const std::vector<string> &authorized_object_ids)
    : DModel("ConfigFile", store, sync, authorized_object_ids),
      contents(this, "contents", true),
      path(this, "path", true),
      pre_update_command(this, "pre_update_command", true),
      post_update_command(this, "post_update_command", true)
{

}
