#!/bin/bash

chmod 600 /etc/rekonf/keys
systemctl enable rekonf.service
systemctl start rekonf.service || /bin/true
