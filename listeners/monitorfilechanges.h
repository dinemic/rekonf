#ifndef MONITORFILECHANGES_H
#define MONITORFILECHANGES_H

#include <libdinemic/daction.h>
#include <libdinemic/dmodel.h>
#include <models/confignode.h>
#include <models/configfile.h>

class MonitorFileChanges : public Dinemic::DAction
{
public:
    MonitorFileChanges();

    void apply(Dinemic::Sync::SyncInterface *sync);
    void revoke(Dinemic::Sync::SyncInterface *sync);

    // Prevent applying unauthorized updates on local database
    void on_unauthorized_update(Dinemic::DActionContext &context, const std::string &key, const std::string &old_value, const std::string &new_value);

    // Handle updates when they are authorized and call pre update command
    void on_authorized_update(Dinemic::DActionContext &context, const std::string &key, const std::string &old_value, const std::string &new_value);

    // Update file contents and call post update command
    void on_authorized_updated(Dinemic::DActionContext &context, const std::string &key, const std::string &old_value, const std::string &new_value);
};

#endif // MONITORFILECHANGES_H
