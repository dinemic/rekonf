
#include "monitorfilechanges.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;


MonitorFileChanges::MonitorFileChanges()
{

}

void MonitorFileChanges::apply(SyncInterface *sync) {
    sync->add_on_update_listener("ConfigFile:[id]:value_contents", this);
    sync->add_on_updated_listener("ConfigFile:[id]:value_contents", this);
}

void MonitorFileChanges::revoke(SyncInterface *sync) {
    sync->remove_on_update_listener("ConfigFile:[id]:value_contents", this);
    sync->remove_on_updated_listener("ConfigFile:[id]:value_contents", this);
}

void MonitorFileChanges::on_unauthorized_update(DActionContext &context, const string &key, const string &old_value, const string &new_value) {
    throw DUpdateRejected("Unauthorized");
}

void MonitorFileChanges::on_authorized_update(DActionContext &context, const std::string &key, const std::string &old_value, const std::string &new_value) {
    if (key != "value_contents") {
        ACTION("Not interested in this field: " + key);
        return;
    }
    
    vector<string> owned_nodes = DModel::object_list_owned("ConfigNode:[id]");
    if (owned_nodes.size() == 0) {
        ACTION("No ConfigNode defined on this machine. Not updating any files.");
        return;
    }

    ConfigNode node(owned_nodes[0], context.get_object().get_store_interface(), context.get_object().get_sync_interface());
    ConfigFile file(context.get_object().get_db_id(), context.get_object().get_store_interface(), context.get_object().get_sync_interface(), &node);

    try {
        if (node.files.index(file.get_db_id()) < 0) {
            ACTION("File " + file.get_db_id() + " not associated with this node: " + node.get_db_id());
            return;
        }
    } catch (exception &e) {
        ERROR("Failed to decrypt field of owned model");
        throw DUpdateRejected("Failed to decrypt file list");
    }

    try {
        if (file.pre_update_command.to_string().size() > 0) {
            ACTION("Calling pre update command: " + file.pre_update_command.to_string());
            string output = Utils::exec(file.pre_update_command.to_string());
            node.file_pre_actions.set(file.get_db_id(), output);
        }
    } catch (exception &e) {
        ERROR("Failed to decrypt pre_update command of owned model");
        throw DUpdateRejected("Failed to decrypt pre_update command");
    }
}

void MonitorFileChanges::on_authorized_updated(DActionContext &context, const string &key, const string &old_value, const string &new_value) {
    if (key != "value_contents") {
        ACTION("Not interested in this field: " + key);
        return;
    } else {
        ACTION("Contents updated");
    }

    vector<string> owned_nodes;
    try {
        owned_nodes = DModel::object_list_owned("ConfigNode:[id]");
        if (owned_nodes.size() < 1) {
            ACTION("No ConfigNode defined on this machine. Ignoring update.");
            return;
        }
        for (string onode : owned_nodes) {
            INFO("Got node: " + onode);
        }
    } catch (exception &e) {
        ERROR("Failed to check owned nodes");
        return;
    }

    ConfigNode node(owned_nodes[0], context.get_object().get_store_interface(), context.get_object().get_sync_interface());
    ConfigFile file(context.get_object().get_db_id(), context.get_object().get_store_interface(), context.get_object().get_sync_interface(), &node);

    try {
        if (node.files.index(file.get_db_id()) < 0) {
            ACTION("File " + file.get_db_id() + " is not associated with this node: " + node.get_db_id());
            return;
        }
    } catch (exception &e) {
        ERROR("Failed to decrypt this node's file list. Ignoring update");
        return;
    }

    INFO("Updating file " + file.get_db_id() + ": " + file.path.to_string());

    try {
        ofstream config_file(file.path.to_string(), ofstream::binary);
        config_file << file.contents.to_string() << endl;
        config_file.close();
    } catch (exception &e) {
        ERROR("Failed to edit file: " + e.what());
    }

    node.file_status.set(file.get_db_id(), context.get_headers()[HEADER_ID]);

    if (file.post_update_command.to_string().size() > 0) {
        ACTION("Calling post update command: " + file.post_update_command.to_string());
        string output = Utils::exec(file.post_update_command.to_string());
        node.file_post_actions.set(file.get_db_id(), output);
    }
}
