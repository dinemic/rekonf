#include <iostream>
#include <rekonfapp.h>

using namespace std;

int main(int argc, char **argv)
{
    RekonfApp app(argc, argv);
    return app.exec();
}
